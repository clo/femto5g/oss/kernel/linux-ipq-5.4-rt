// SPDX-License-Identifier: (GPL-2.0+ OR BSD-3-Clause)
/*
 * IPQ5332 DB-MI01.1 board device tree source
 *
 * Copyright (c) 2020-2021 The Linux Foundation. All rights reserved.
 * Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
 */

/dts-v1/;

#include "ipq5332.dtsi"

#ifdef __IPQ_MEM_PROFILE_512_MB__
#include "ipq5332-512MB-memory.dtsi"
#else
#include "ipq5332-default-memory.dtsi"
#endif

/ {
	#address-cells = <0x2>;
	#size-cells = <0x2>;
	model = "Qualcomm Technologies, Inc. IPQ5332/DB-MI01.1";
	compatible = "qcom,ipq5332-db-mi01.1", "qcom,ipq5332";
	interrupt-parent = <&intc>;

	aliases {
		serial0 = &blsp1_uart0;
	};

	chosen {
		stdout-path = "serial0";
		bootargs-append = " clk_ignore_unused";
	};

	soc {
		pinctrl@1000000 {
			spi_0_pins: spi0-pinmux {
				spi_clock {
					pins = "gpio14";
					function = "blsp0_spi";
					drive-strength = <8>;
					bias-pull-down;
				};

				spi_mosi {
					pins = "gpio15";
					function = "blsp0_spi";
					drive-strength = <8>;
					bias-pull-down;
				};

				spi_miso {
					pins = "gpio16";
					function = "blsp0_spi";
					drive-strength = <8>;
					bias-pull-down;
				};

				spi_cs {
					pins = "gpio17";
					function = "blsp0_spi";
					drive-strength = <8>;
					bias-pull-up;
				};
			};

			serial_0_pins: serial0-pinmux {
				pins = "gpio18", "gpio19";
				function = "blsp0_uart0";
				drive-strength = <8>;
				bias-pull-up;
			};

			leds_pins: leds_pinmux {
				led0_2g {
					pins = "gpio36";
					function = "gpio";
					drive-strength = <8>;
					bias-pull-down;
				};
			};
		};

		serial@78af000 {
			pinctrl-0 = <&serial_0_pins>;
			pinctrl-names = "default";
			status = "ok";
		};

		spi@78b5000 {
			pinctrl-0 = <&spi_0_pins>;
			pinctrl-names = "default";
			cs-select = <0>;
			status = "ok";

			m25p80@0 {
				compatible = "n25q128a11";
				#address-cells = <1>;
				#size-cells = <1>;
				reg = <0>;
				spi-max-frequency = <50000000>;
			};
		};

		dma@7984000 {
			status = "ok";
		};

		nand: nand@79b0000 {
			pinctrl-0 = <&qspi_nand_pins>;
			pinctrl-names = "default";
			status = "ok";
		};

		usb3@8A00000 {
			status = "ok";

			dwc_0: dwc3@8A00000 {
				/delete-property/ #phy-cells;
				/delete-property/ phys;
				/delete-property/ phy-names;
			};
		};

		hs_m31phy_0: hs_m31phy@7b000 {
			status = "ok";
		};

		leds {
			compatible = "gpio-leds";
			pinctrl-0 = <&leds_pins>;
			pinctrl-names = "default";
			led@36 {
				label = "led0_2g";
				gpios = <&tlmm 36 GPIO_ACTIVE_HIGH>;
				linux,default-trigger = "led_2g";
				default-state = "off";
			};
		};

		pcie0_phy: phy@4b0000 {
			status = "ok";
		};

		pcie1_phy_x2: phy_x2@4b1000 {
			status = "ok";
		};

		pcie1: pcie@18000000 {
			max-link-speed = <1>;
			perst-gpio = <&tlmm 47 GPIO_ACTIVE_LOW>;
			status = "ok";
			pcie1_rp {
				reg = <0 0 0 0 0>;

				qcom,mhi@1 {
					reg = <0 0 0 0 0>;
				};
			};
		};

		pcie0: pcie@20000000 {
			max-link-speed = <1>;
			perst-gpio = <&tlmm 38 GPIO_ACTIVE_LOW>;
			status = "ok";
			pcie0_rp {
				reg = <0 0 0 0 0>;

				qcom,mhi@2 {
					reg = <0 0 0 0 0>;
				};
			};
		};
	};
};
